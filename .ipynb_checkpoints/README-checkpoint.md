# 地铁站名归纳
## 0 文件列表
- **Subway.ipynb** 为对于北京地铁站名各种方面的归纳
- 未完待续

## 1 使用方法
### 1.1 Google Colab （可能需要更加科学的上网方式）
打开Google colab 并直接导入此文件，按顺序运行即可

### 1.2 本地 Jupyter notebook
- 百度一下如何安装**anaconda**
- 下载最新版安装之
- 进入**anaconda**并安装里面的**jupyter notebook**
- 注意配置**python** 版本为 **3.x.x**
- 继续百度如何进入**jupyter notebook**
- 进入之
- 打开*Subway.ipynb*
- 按序运行之（其中 **pip install pinyin** 需要去掉，并在anaconda的你所运行的环境中使用该语句安装pinyin这个包）

## 2 相关视频链接
- B站：
    - 第一集  [https://www.bilibili.com/video/av89553183/](https://www.bilibili.com/video/av89553183/)
    - 第二集 [https://www.bilibili.com/video/BV1uE411J7mM/](https://www.bilibili.com/video/BV1uE411J7mM/)
- YouTube:
    - 第一集 [https://youtu.be/h3zS17i5D1A](https://youtu.be/h3zS17i5D1A)
    - 第二集 [https://youtu.be/vLr3cPoZwN8](https://youtu.be/vLr3cPoZwN8)
